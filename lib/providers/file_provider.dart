import 'dart:io';

import 'package:convert_json_csv_desktop/models/export/csv_file.dart';
import 'package:flutter/material.dart';

class FileProvider extends ChangeNotifier {
  File? importedFile;
  String jsonContent = '';
  List<CsvFile> lines = [];

  void setImportedFile(File file) {
    importedFile = file;
    notifyListeners();
  }

  void setJsonContent(String content) {
    jsonContent = content;
    notifyListeners();
  }

  void setCsvLines(List<CsvFile> lines) {
    this.lines = lines;
    notifyListeners();
  }
}
