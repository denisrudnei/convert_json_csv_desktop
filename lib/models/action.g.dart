// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'action.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Action _$ActionFromJson(Map<String, dynamic> json) => Action(
      id: json['id'] as String?,
      idMemberCreator: json['idMemberCreator'] as String?,
      data: json['data'] == null
          ? null
          : Data.fromJson(json['data'] as Map<String, dynamic>),
      type: json['type'] as String?,
      date: json['date'] as String?,
      memberCreator: json['memberCreator'] == null
          ? null
          : MemberCreator.fromJson(
              json['memberCreator'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ActionToJson(Action instance) => <String, dynamic>{
      'id': instance.id,
      'idMemberCreator': instance.idMemberCreator,
      'data': instance.data,
      'type': instance.type,
      'date': instance.date,
      'memberCreator': instance.memberCreator,
    };
