import 'dart:developer';
import 'dart:math';

import 'package:convert_json_csv_desktop/models/acitivity.dart';
import 'package:convert_json_csv_desktop/models/action.dart';
import 'package:convert_json_csv_desktop/models/category_of_work.dart';
import 'package:convert_json_csv_desktop/models/card.dart';
import 'package:convert_json_csv_desktop/models/card_list.dart';
import 'package:convert_json_csv_desktop/models/export/csv_file.dart';
import 'package:convert_json_csv_desktop/models/label.dart';
import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:collection/collection.dart'; 

class CsvController {
  final Map<String, CardList> board;

  CsvController({required this.board});

  List<CsvFile> convertToCsv() {
    List<CsvFile> lines = [];
    Map<String, Activity> activitys = convertCardsToActivity();
   
    board.forEach((key, listCards) {
      if(listCards.name.toLowerCase() == "reference"){
        activitys.forEach((codeActivity, activity) {
          Card? cardReference = listCards.cards?.firstWhereOrNull(
          (card) => card.id == activity.id);
          if(cardReference != null){
            String template = cardTemplateAtividade();
            template += "|";
            template += activity.services.where((element) => element.name.isNotEmpty)
            .map((e) => cardTemplateHistoria(e.name),).toList().join("\n"); 
            String result = activity.convertToCsvActivity(template);
            lines.add(
              CsvFile(
                  list: listCards.name,
                  title: result.split("|")[0],
                  description: cardReference.desc ?? "",
                  points: '',
                  due: cardReference.dateConclusionFormated(),
                  members: cardReference.members!.map((e) => e.initials).toList().join(","),
                  labels: cardReference.labels!
                      .map((e) => e.name)
                      .toList()
                      .join(','),
                  cardId: cardReference.id!,
                  cardUrl: cardReference.url!,
                )
            );
            List<String> historys = result.split("|")[1].split("\n");
            historys.removeWhere((element) => element.isEmpty);
            for(int i = 0; i < historys.length; i++){
              lines.add(
              CsvFile(
                  list: activity.services[i].status,
                  title: historys[i],
                  description: cardReference.desc ?? "",
                  points: '',
                  due: activity.services[i].dateConclusionFormated(),
                  members: activity.services[i].worker,
                  labels: activity.services[i].name,
                  cardId: cardReference.id!,
                  cardUrl: cardReference.url!,
                )
            );
            }
            List<String> impacts = activity.convertToCsvImpact(cardTemplateImpact()).split("\n");
            for(int i = 0; i < activity.services.length; i++){
              if(activity.services[i].name.isNotEmpty){
                lines.add(
                  CsvFile(
                      list: "IMPACTO",
                      title: impacts[i],
                      description: cardReference.desc ?? "",
                      points: '',
                      due:"",
                      members: activity.services[i].worker,
                      labels: activity.services[i].name,
                      cardId: activity.id,
                      cardUrl: cardReference.url!,
                    )
                );
              }
            }
          }
        });

        for(Card card in listCards.cards!){
          Label? effort  = card.labels?.firstWhereOrNull((e)=> e.name.toLowerCase() == "esforço");
          if(effort != null){
            lines.add(
              CsvFile(
                  list: listCards.name,
                  title: extractReservedTime(card,cardTemplateEsforco()),
                  description: card.desc ?? "",
                  points: '',
                  due: "",
                  members: card.members!.map((e) => e.initials).toList().join(","),
                  labels: card.labels!
                      .map((e) => e.name)
                      .toList()
                      .join(','),
                  cardId: card.id!,
                  cardUrl: card.url!,
                )
            );
          }
        }
      }
      else{
        for(Card card in listCards.cards!){
          Label? hotfix  = 
          card.labels?.firstWhereOrNull((e)=> e.name.toLowerCase() == "hotfix");
          if(hotfix != null){
            lines.add(
              CsvFile(
                  list: listCards.name,
                  title: card.name ?? "",
                  description: card.desc ?? "",
                  points: '',
                  due: "",
                  members: card.members!.map((e) => e.initials).toList().join(","),
                  labels: card.labels!
                      .map((e) => e.name)
                      .toList()
                .join(','),
                  cardId: card.id!,
                  cardUrl: card.url!,
                )
            );
          }
        }
      }
    });
    return lines;
  }

  Map<String, Activity> convertCardsToActivity() {
    CardList reference = board.values
        .firstWhere((element) => element.name.toLowerCase() == "reference");
    Map<String, Activity> codesActivities =
        _listCodesActivities(reference.cards!);
    List<Card> allCardsOfActivities = [];
    board.forEach((key, value) {
      if (key != reference.id) {
        allCardsOfActivities.addAll(value.cards!);
      }
    });
    for (Card card in allCardsOfActivities) {
      RegExpMatch? match = RegExp(r'^\d+').firstMatch(card.name!);
      if (match != null) {
        String codeActivity = card.name!.substring(match.start, match.end);
        if (codesActivities.containsKey(codeActivity)) {
          List<CategoryOfWork> services =
              codesActivities[codeActivity]!.services;
          CategoryOfWork? serviceMatched =
              _searchByServiceTag(card.labels!, services);
          if (serviceMatched != null) {
            serviceMatched.tags = card.readAllCommentsAndSeparateByTags();
            serviceMatched.status = board[card.idList]!.name;
            serviceMatched.dateConclusion = card.due ?? "";
            serviceMatched.worker = card.members?.first.initials ?? "";
            card.members?.forEach((worker) {
              codesActivities[codeActivity]!.addWorker(worker);
            });
          }
        }
      }
    }
    return codesActivities;
  }

  String extractReservedTime(Card card, String template,{String separator = "\n"}){
    List<Action> comments = card.comments ?? [];
    Map<String,int> tags = {};
    for(var comment in comments){
      if((comment.data?.text ?? "").isNotEmpty){
        RegExp(r'^([A-z\s]*?):(\s*?\d{1,2}[h\:])([0-5][0-9])?')
        .allMatches(comment.data?.text ?? "").forEach((match) {
          if(match.groupCount > 2){
            String? key = match.group(1);
            if(key != null){
              int secondsWorked = 0;
              String? value = match.group(2)?.replaceAll(RegExp(r'[^0-9]'), "");
              secondsWorked = (int.parse(value ?? "0")*60)*60;
              String? value2 = match.group(3)?.replaceAll(RegExp(r'[^0-9]'), "");
              if(value2 != null){
                secondsWorked += int.parse(value2)*60;
              }
              if(tags.containsKey(key)){
                tags[key] = (tags[key]!) + secondsWorked;
              }
              else{
                tags[key] = secondsWorked;
              } 
            }
          }
        });
      }   
    }
    List<String> serviceTemplate = [];
    template.split(separator).forEach((line) {
    String resultLine = line;
    tags.forEach((key, value) {
        RegExp pattern = RegExp(r"\{\{"+key+r":\}\}", caseSensitive: false);
        int hours = (value/60)~/60;
        int minutes = (value - ((hours*60)*60))~/60;
        resultLine = resultLine.replaceAll(pattern, "${hours.toString().padLeft(2,"0")}:${minutes.toString().padLeft(2,"0")}");
      });
    RegExp patternTag = RegExp(r"\{\{\w*:\}\}", caseSensitive: false);
    if(patternTag.hasMatch(resultLine)){
      resultLine = resultLine.replaceAll(patternTag, "00:00");
    }
    serviceTemplate.add(resultLine.trim());
    });
    String fullLine = serviceTemplate.join("");
    return fullLine;
  }

  Map<String,Activity> _listCodesActivities(List<Card> cards){
    Map<String,Activity> activities = {};
    for (Card card in cards){
      bool cardIsActivity = false;
      for (Label label in card.labels!) {
        if (label.name.toLowerCase() == "atividade" && !(card.isTemplate!)) {
          cardIsActivity = true;
          break;
        }
      }
      if (cardIsActivity) {
        RegExpMatch? match = RegExp(r'^\d+').firstMatch(card.name!);
        if (match != null) {
          Activity activity = Activity();
          activity.description = card.name!.replaceAll(RegExp(r'^[^A-z]+'), "");
          activity.code = card.name!.substring(match.start, match.end);
          activity.id = card.id ?? "";
          RegExp(r'(\[.+?:.+?\])').allMatches(card.desc!).forEach((meta) {
            String data = card.desc!.substring(meta.start + 1, meta.end - 1);
            List<String> keyAndValue = data.split(":");

            //Programação, Teste, analise, etc
            String category = md5
                .convert(utf8.encode(keyAndValue[0].toLowerCase().trim()))
                .toString();
            //Tempo estimado
            String estimatedTime = keyAndValue[1].trim();
            String hours = estimatedTime
            .replaceAll(RegExp(r'^(\s*?\d{1,2})[h\:]([0-5][0-9])?'), r"$1");
            String minutes = "";

            if(estimatedTime.split(RegExp(r"[h\:]")).length > 1){
              minutes = estimatedTime
              .replaceAll(RegExp(r'^(\s*?\d{1,2})[h\:]([0-5][0-9])?'), r"$2");
            }
            

            CategoryOfWork categoryOfWork = CategoryOfWork();
            categoryOfWork.codeCategory = category;
            categoryOfWork.estimatedTimeInMinutes = 
              ((int.tryParse(hours) ?? 0)*60) + (int.tryParse(minutes) ?? 0);
            // categoryOfWork.worker = 

            activity.services.add(categoryOfWork);
            activities[activity.code] = activity;
          });
        }
      }
    }
    return activities;
  }

  CategoryOfWork? _searchByServiceTag(
      List<Label> labels, List<CategoryOfWork> services) {
    int cont = 0;
    CategoryOfWork? serviceMatched;
    for (Label label in labels) {
      String nameEnconded =
          CategoryOfWork.convertTextInCodeCategory(label.name);
      for (CategoryOfWork service in services) {
        if (service.codeCategory == nameEnconded) {
          service.name = label.name;
          serviceMatched = service;
          cont += 1;
        }
      }
    }
    if (cont > 1) {
      Exception(
          "O card possui mais de uma etiqueta de tempo estimado: exemplo WEB e Teste");
    }
    return serviceMatched;
  }

  String cardTemplateAtividade() {
    return """{{@cod}};
    Análise;{{[Análise]}};{{T:}};00:00;
    Web;{{[Web]}};{{T:}};00:00;
    Programação;{{[Programação]}};{{T:}};48:00;
    Teste;{{[Teste]}};{{T:}};00:00;{{@des}}
    """;
  }

  String cardTemplateHistoria(String tag) {
    return """{{@cod}};H1;E:{{[$tag]}};C:{{T:}};R:00:00;{{@des}}""";
  }

  String cardTemplateImpact(){
    return """Ambiente;{{A:}};
    Correções;{{C:}};
    Reuniões{{R:}};
    Treinamento;{{TR:}};
    Auxiliando_Equipe;{{AE:}};
    Outras_Sprints;{{S:}};
    Outros;{{O:}}""";
  }
  
  String cardTemplateEsforco(){
    return """Auxilio Analise;{{Aa:}};
    Auxilio Web;{{Aw:}};
    Auxilio Java;{{Aj:}};
    Auxilio Teste;{{At:}};
    Auxilio Mobile;{{Am:}};
    Daily;{{Da:}};
    Treinamento;{{Tr:}};
    Automatizado;{{Au:}};
    Correções;{{Co:}}""";
  }
}
