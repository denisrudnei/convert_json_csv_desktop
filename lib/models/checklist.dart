import 'package:convert_json_csv_desktop/models/check_item.dart';
import 'package:json_annotation/json_annotation.dart';

part 'checklist.g.dart';

@JsonSerializable()
class Checklist {
  List<CheckItem>? checkItems;
  String? id;
  String? name;
  String? idBoard;
  String? idCard;
  int? pos;

  Checklist({
    this.checkItems,
    this.id,
    this.name,
    this.idBoard,
    this.idCard,
    this.pos,
  });

  factory Checklist.fromJson(Map<String, dynamic> json) =>
      _$ChecklistFromJson(json);

  Map<String, dynamic> toJson() => _$ChecklistToJson(this);
}
