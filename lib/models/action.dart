import 'package:convert_json_csv_desktop/models/data.dart';
import 'package:convert_json_csv_desktop/models/member_creator.dart';
import 'package:json_annotation/json_annotation.dart';

part 'action.g.dart';

@JsonSerializable()
class Action {
  String? id;
  String? idMemberCreator;
  Data? data;
  String? type;
  String? date;
  MemberCreator? memberCreator;

  Action(
      {this.id,
      this.idMemberCreator,
      this.data,
      this.type,
      this.date,
      this.memberCreator});

  factory Action.fromJson(Map<String, dynamic> json) => _$ActionFromJson(json);

  Map<String, dynamic> toJson() => _$ActionToJson(this);
}
