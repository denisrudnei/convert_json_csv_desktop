import 'package:convert_json_csv_desktop/models/category_of_work.dart';
import 'package:convert_json_csv_desktop/models/member_creator.dart';

class Activity{
  String id = "";
  String code = "";
  String description = "";
  List<CategoryOfWork> services = [];
  Map<String,MemberCreator> workers = {};

  void addWorker(MemberCreator worker){
    if(worker.id != null){
      if(!workers.containsKey(worker.id)){
        workers[worker.id!] = worker;
      }
    }
  }

  String convertToCsvActivity(String template,{String separator = "\n"}){
    List<String> result = [];
    template.split(separator).forEach((line) {
      String resultLine = line;
      RegExp patternCode = RegExp(r'\{\{@cod\}\}');
      RegExp patternDescription = RegExp(r'\{\{@des\}\}');
      if(patternCode.hasMatch(line)){
        resultLine = line.replaceAll(patternCode, code).trim();
        line = resultLine;
      }
      if(patternDescription.hasMatch(line)){
        resultLine = line.replaceAll(patternDescription, description).trim();
        line = resultLine;
      }
      RegExpMatch? match = RegExp(r'\{\{\[.*\]\}\}').firstMatch(line);
      if(match != null){
        String interpolate = line.substring(match.start,match.end);
        interpolate = interpolate.replaceAll(RegExp(r'\{|\}'), "");
        interpolate = interpolate.replaceAll(RegExp(r'\[|\]'), "");
        CategoryOfWork service = services.firstWhere(
          (s) => s.codeCategory == CategoryOfWork
          .convertTextInCodeCategory(interpolate)
        );

        int h = service.estimatedTimeInMinutes~/60;
        int m = (((service.estimatedTimeInMinutes/60) - h)*60).round();
        resultLine = line
        .replaceAll(RegExp(r'\{\{\[.*\]\}\}'), 
        "${h.toString().padLeft(2,"0")}:${m.toString().padLeft(2,"0")}");

        service.tags.forEach((key, value) {
          RegExp pattern = RegExp(r"\{\{"+key+r":\}\}", caseSensitive: false);
          int hours = (value/60)~/60;
          int minutes = (value - ((hours*60)*60))~/60;
          resultLine = resultLine.replaceAll(pattern, "${hours.toString().padLeft(2,"0")}:${minutes.toString().padLeft(2,"0")}");
        });

        RegExp patternTag = RegExp(r"\{\{\w*:\}\}", caseSensitive: false);
        if(patternTag.hasMatch(resultLine)){
          resultLine = resultLine.replaceAll(patternTag, "00:00");
        }
      }
      result.add(resultLine.trim());
    });
    return result.join(separator);    
  }

  String convertToCsvImpact(String template,{String separator = "\n"}){
    List<String> result = [];
    for(CategoryOfWork service in services){
      List<String> serviceTemplate = [];
      template.split(separator).forEach((line) {
      String resultLine = line;
      service.tags.forEach((key, value) {
          RegExp pattern = RegExp(r"\{\{"+key+r":\}\}", caseSensitive: false);
          int hours = (value/60)~/60;
          int minutes = (value - ((hours*60)*60))~/60;
          resultLine = resultLine.replaceAll(pattern, "${hours.toString().padLeft(2,"0")}:${minutes.toString().padLeft(2,"0")}");
        });
      RegExp patternTag = RegExp(r"\{\{\w*:\}\}", caseSensitive: false);
      if(patternTag.hasMatch(resultLine)){
        resultLine = resultLine.replaceAll(patternTag, "00:00");
      }
      serviceTemplate.add(resultLine.trim());
      });
      String fullLine = serviceTemplate.join("");
      result.add(fullLine);
    }
    return result.join(separator);
  }
}
