import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:intl/intl.dart';


class CategoryOfWork {
  static String convertTextInCodeCategory(String value) {
    return md5.convert(utf8.encode(value.toLowerCase().trim())).toString();
  }

  String codeCategory = "";
  String name = "";
  int estimatedTimeInMinutes = 0;
  String timeWorked = "";
  String worker = "";
  String status = "";
  String dateConclusion = "";
  Map<String, int> tags = {};

  String dateConclusionFormated(){
    try {
      DateTime dt = DateTime.parse(dateConclusion);
      return DateFormat('dd/MM/yyyy HH:mm').format(dt);
    } on Exception catch (_) {
      return "";
    }
  }
}
