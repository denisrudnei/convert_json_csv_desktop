// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'checklist.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Checklist _$ChecklistFromJson(Map<String, dynamic> json) => Checklist(
      checkItems: (json['checkItems'] as List<dynamic>?)
          ?.map((e) => CheckItem.fromJson(e as Map<String, dynamic>))
          .toList(),
      id: json['id'] as String?,
      name: json['name'] as String?,
      idBoard: json['idBoard'] as String?,
      idCard: json['idCard'] as String?,
      pos: json['pos'] as int?,
    );

Map<String, dynamic> _$ChecklistToJson(Checklist instance) => <String, dynamic>{
      'checkItems': instance.checkItems,
      'id': instance.id,
      'name': instance.name,
      'idBoard': instance.idBoard,
      'idCard': instance.idCard,
      'pos': instance.pos,
    };
