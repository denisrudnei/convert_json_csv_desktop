// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Card _$CardFromJson(Map<String, dynamic> json) => Card(
      id: json['id'] as String?,
      desc: json['desc'] as String?,
      name: json['name'] as String?,
      idList: json['idList'] as String?,
      url: json['url'] as String? ?? "",
      labels: (json['labels'] as List<dynamic>?)
              ?.map((e) => Label.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
    )
      ..closed = json['closed'] as bool?
      ..dueComplete = json['dueComplete'] as bool?
      ..dateLastActivity = json['dateLastActivity'] as String?
      ..due = json['due'] as String?
      ..dueReminder = json['dueReminder'] as int?
      ..email = json['email'] as String?
      ..idBoard = json['idBoard'] as String?
      ..idChecklists = (json['idChecklists'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList()
      ..idLabels =
          (json['idLabels'] as List<dynamic>?)?.map((e) => e as String).toList()
      ..idMembers = (json['idMembers'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList()
      ..idShort = json['idShort'] as int?
      ..idAttachmentCover = json['idAttachmentCover'] as String?
      ..manualCoverAttachment = json['manualCoverAttachment'] as bool?
      ..pos = (json['pos'] as num?)?.toDouble()
      ..shortLink = json['shortLink'] as String?
      ..shortUrl = json['shortUrl'] as String?
      ..start = json['start'] as String?
      ..subscribed = json['subscribed'] as bool?
      ..cover = json['cover'] as Map<String, dynamic>?
      ..isTemplate = json['isTemplate'] as bool?;

Map<String, dynamic> _$CardToJson(Card instance) => <String, dynamic>{
      'id': instance.id,
      'closed': instance.closed,
      'dueComplete': instance.dueComplete,
      'dateLastActivity': instance.dateLastActivity,
      'desc': instance.desc,
      'due': instance.due,
      'dueReminder': instance.dueReminder,
      'email': instance.email,
      'idBoard': instance.idBoard,
      'idChecklists': instance.idChecklists,
      'idLabels': instance.idLabels,
      'idList': instance.idList,
      'idMembers': instance.idMembers,
      'idShort': instance.idShort,
      'idAttachmentCover': instance.idAttachmentCover,
      'labels': instance.labels,
      'manualCoverAttachment': instance.manualCoverAttachment,
      'name': instance.name,
      'pos': instance.pos,
      'shortLink': instance.shortLink,
      'shortUrl': instance.shortUrl,
      'start': instance.start,
      'subscribed': instance.subscribed,
      'url': instance.url,
      'cover': instance.cover,
      'isTemplate': instance.isTemplate,
    };
