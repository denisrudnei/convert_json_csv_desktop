// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'check_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CheckItem _$CheckItemFromJson(Map<String, dynamic> json) => CheckItem(
      idChecklist: json['idChecklist'] as String?,
      state: json['state'] as String?,
      id: json['id'] as String?,
      name: json['name'] as String?,
      pos: json['pos'] as int?,
      idMember: json['idMember'] as String?,
    );

Map<String, dynamic> _$CheckItemToJson(CheckItem instance) => <String, dynamic>{
      'idChecklist': instance.idChecklist,
      'state': instance.state,
      'id': instance.id,
      'name': instance.name,
      'pos': instance.pos,
      'idMember': instance.idMember,
    };
