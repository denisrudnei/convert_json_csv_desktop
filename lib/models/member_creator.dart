import 'package:json_annotation/json_annotation.dart';

part 'member_creator.g.dart';

@JsonSerializable()
class MemberCreator {
  String? id;
  bool? activityBlocked;
  String? avatarHash;
  String? avatarUrl;
  String? fullName;
  String? initials;
  bool? nonPublicAvailable;
  String? username;

  MemberCreator(
      {this.id,
      this.activityBlocked,
      this.avatarHash,
      this.avatarUrl,
      this.fullName,
      this.initials,
      this.nonPublicAvailable,
      this.username});

  factory MemberCreator.fromJson(Map<String, dynamic> json) =>
      _$MemberCreatorFromJson(json);

  Map<String, dynamic> toJson() => _$MemberCreatorToJson(this);
}
