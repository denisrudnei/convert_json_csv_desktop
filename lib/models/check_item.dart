import 'package:json_annotation/json_annotation.dart';

part 'check_item.g.dart';

@JsonSerializable()
class CheckItem {
  String? idChecklist;
  String? state;
  String? id;
  String? name;
  int? pos;
  String? idMember;

  CheckItem({
    this.idChecklist,
    this.state,
    this.id,
    this.name,
    this.pos,
    this.idMember,
  });

  factory CheckItem.fromJson(Map<String, dynamic> json) =>
      _$CheckItemFromJson(json);

  Map<String, dynamic> toJson() => _$CheckItemToJson(this);
}
