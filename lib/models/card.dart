import 'package:convert_json_csv_desktop/models/action.dart';
import 'package:convert_json_csv_desktop/models/card_list.dart';
import 'package:convert_json_csv_desktop/models/checklist.dart';
import 'package:convert_json_csv_desktop/models/label.dart';
import 'package:convert_json_csv_desktop/models/member_creator.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:intl/intl.dart';

part 'card.g.dart';

@JsonSerializable()
class Card {
  String? id;
  bool? closed;
  bool? dueComplete;
  String? dateLastActivity;
  String? desc;
  String? due;
  int? dueReminder;
  String? email;
  String? idBoard;
  List<String>? idChecklists;
  List<String>? idLabels;
  String? idList;
  List<String>? idMembers;
  int? idShort;
  String? idAttachmentCover;
  List<Label>? labels;
  bool? manualCoverAttachment;
  String? name;
  double? pos;
  String? shortLink;
  String? shortUrl;
  String? start;
  bool? subscribed;
  String? url;
  Map<String, dynamic>? cover;
  bool? isTemplate;
  @JsonKey(ignore: true)
  List<Action>? comments = [];
  @JsonKey(ignore: true)
  List<Checklist>? checklist = [];
  @JsonKey(ignore: true)
  List<MemberCreator>? members = [];

  Card({
    required this.id,
    required this.desc,
    required this.name,
    required this.idList,
    this.url = "",
    this.labels = const [],
  });

  factory Card.fromJson(Map<String, dynamic> json) => _$CardFromJson(json);
  Map<String, dynamic> toJson() => _$CardToJson(this);

  Map<String,int> readAllCommentsAndSeparateByTags(){
    Map<String,int> tags = {};
    if(comments != null){
      for (Action comment in comments!){
        RegExp(r'^([A-z\s]*?):(\s*?\d{1,2}[h\:])([0-5][0-9])?')
        .allMatches(comment.data?.text ?? "")
        .forEach((match) {
          if(match.groupCount > 2){
            String? key = match.group(1);
            if (key != null) {
              int secondsWorked = 0;
              String? value = match.group(2)?.replaceAll(RegExp(r'[^0-9]'), "");
              secondsWorked = (int.parse(value ?? "0") * 60) * 60;
              String? value2 =
                  match.group(3)?.replaceAll(RegExp(r'[^0-9]'), "");
              if (value2 != null) {
                secondsWorked += int.parse(value2) * 60;
              }
              if (tags.containsKey(key)) {
                tags[key] = (tags[key]!) + secondsWorked;
              } else {
                tags[key] = secondsWorked;
              }
            }
          }
        });
      }
    }
    return tags;
  }

  String dateConclusionFormated(){
    if(due != null){
      DateTime? dt = DateTime.tryParse(due!);
      if(dt != null) return DateFormat('dd/MM/yyyy HH:mm').format(dt);
    }
    return "";
  }
}
