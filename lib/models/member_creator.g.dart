// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'member_creator.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MemberCreator _$MemberCreatorFromJson(Map<String, dynamic> json) =>
    MemberCreator(
      id: json['id'] as String?,
      activityBlocked: json['activityBlocked'] as bool?,
      avatarHash: json['avatarHash'] as String?,
      avatarUrl: json['avatarUrl'] as String?,
      fullName: json['fullName'] as String?,
      initials: json['initials'] as String?,
      nonPublicAvailable: json['nonPublicAvailable'] as bool?,
      username: json['username'] as String?,
    );

Map<String, dynamic> _$MemberCreatorToJson(MemberCreator instance) =>
    <String, dynamic>{
      'id': instance.id,
      'activityBlocked': instance.activityBlocked,
      'avatarHash': instance.avatarHash,
      'avatarUrl': instance.avatarUrl,
      'fullName': instance.fullName,
      'initials': instance.initials,
      'nonPublicAvailable': instance.nonPublicAvailable,
      'username': instance.username,
    };
