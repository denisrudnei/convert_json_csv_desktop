import 'dart:convert';
import 'dart:io';

import 'package:convert_json_csv_desktop/models/action.dart';
import 'package:convert_json_csv_desktop/models/card.dart';
import 'package:convert_json_csv_desktop/models/card_list.dart';
import 'package:convert_json_csv_desktop/models/checklist.dart';
import 'package:convert_json_csv_desktop/models/export/csv_controller.dart';
import 'package:convert_json_csv_desktop/models/export/csv_file.dart';
import 'package:convert_json_csv_desktop/models/member_creator.dart';
import 'package:convert_json_csv_desktop/providers/file_provider.dart';
import 'package:file_picker/file_picker.dart';
import 'package:open_file/open_file.dart';
import 'package:syncfusion_flutter_xlsio/xlsio.dart';

class ImportService {
  final FileProvider provider;
  ImportService({required this.provider});

  Future<void> importFile() async {
    FilePickerResult? file = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['json'],
      dialogTitle: 'Selecionar arquivo para importar',
    );
    if (file == null) return;
    String content = await File(file.files.first.path!).readAsString();
    provider.setImportedFile(File(file.files.first.path!));
    var encoder = const JsonEncoder.withIndent('    ');
    var json = jsonDecode(content);
    provider.setJsonContent(encoder.convert(json));

    var board = await convertFileToLists(json);
    CsvController csvController = CsvController(board: board);
    provider.setCsvLines(csvController.convertToCsv());
  }

  Future<Map<String, CardList>> convertFileToLists(
      Map<String, dynamic> json) async {
    List<Card> cards =
        (json['cards'] as List<dynamic>).map((e) => Card.fromJson(e)).toList();
    List<Action> actions = (json['actions'] as List<dynamic>)
        .map((e) => Action.fromJson(e))
        .toList();
    Map<String, List<Action>> comments = {};
    actions.forEach((action) {
      if ((action.data?.text ?? "").isNotEmpty) {
        String key = action.data!.card!["id"]! as String;
        if (!comments.containsKey(key)) {
          comments[key] = <Action>[];
        }
        comments[key]!.add(action);
      }
    });
    List<Checklist> cardChecklists = (json['checklists'] as List<dynamic>)
        .map((e) => Checklist.fromJson(e))
        .toList();
    Map<String, List<Checklist>> checklists = {};
    cardChecklists.forEach((checklist) {
      if ((checklist.idCard ?? "").isNotEmpty) {
        String key = checklist.idCard!;
        if (!checklists.containsKey(key)) {
          checklists[key] = <Checklist>[];
        }
        checklists[key]!.add(checklist);
      }
    });
    Map<String, CardList> board = {};
    (json['lists'] as List<dynamic>).forEach((element) {
      if(element["closed"] == false){
        board[element["id"]] = CardList(id: element["id"], name: element["name"]);
      }
    });
    List<MemberCreator> members = [];
    (json['members'] as List<dynamic>).forEach((m) {
      members.add(MemberCreator(
        id: m["id"],
        fullName: m["fullName"],
        initials: m["initials"],
        username: m["username"],
      ));
    });
    for (var card in cards) {
      card.comments = comments[card.id];
      card.checklist = checklists[card.id];
      card.idMembers?.forEach((idMember) {
        MemberCreator member = members.firstWhere((m) => m.id == idMember);
        card.members?.add(member);
      });
      board[card.idList]?.cards!.add(card);
    }
    return board;
  }

  Future<List<Card>> convertCardsFromLists(List<CardList> lists) async {
    return lists.map((e) => e.cards ?? []).expand((i) => i).toList();
  }

  Future<void> exportCsv() async {
    var path = await FilePicker.platform.saveFile(
      allowedExtensions: ['xlsx'],
      fileName: 'export.xlsx',
    );
    if (path == null) return;
    File file = File(path);
    CsvFile header = CsvFile(
      list: 'List',
      title: 'Title',
      description: 'Description',
      points: 'Points',
      due: 'Due',
      members: 'Members',
      labels: 'Labels',
      cardId: 'Card #',
      cardUrl: 'Card URL',
    );
    final Workbook workbook = Workbook();
    Worksheet sheet = workbook.worksheets[0];
    List<CsvFile> allLines = [header, ...provider.lines];

    sheet.importData(
        provider.lines
            .map(
              (line) => ExcelDataRow(
                cells: [
                  ExcelDataCell(
                    columnHeader: 'List',
                    value: line.list,
                  ),
                  ExcelDataCell(
                    columnHeader: 'Title',
                    value: line.title,
                  ),
                  ExcelDataCell(
                    columnHeader: 'Description',
                    value: line.description,
                  ),
                  ExcelDataCell(
                    columnHeader: 'Points',
                    value: line.points,
                  ),
                  ExcelDataCell(
                    columnHeader: 'Due',
                    value: line.due,
                  ),
                  ExcelDataCell(
                    columnHeader: 'Members',
                    value: line.members,
                  ),
                  ExcelDataCell(
                    columnHeader: 'Labels',
                    value: line.labels,
                  ),
                  ExcelDataCell(
                    columnHeader: 'Card #',
                    value: line.cardId,
                  ),
                  ExcelDataCell(
                    columnHeader: 'Card URL',
                    value: line.cardUrl,
                  ),
                ],
              ),
            )
            .toList(),
        1,
        1);
    file.writeAsBytes(workbook.saveAsStream());
    OpenFile.open(file.path);
  }
}
