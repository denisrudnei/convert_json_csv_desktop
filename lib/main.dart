import 'package:convert_json_csv_desktop/providers/file_provider.dart';
import 'package:convert_json_csv_desktop/services/import_service.dart';
import 'package:convert_json_csv_desktop/util/screen/colors.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(
        create: (_) => FileProvider(),
      ),
      Provider(
        create: (context) {
          FileProvider provider = Provider.of<FileProvider>(
            context,
            listen: false,
          );
          return ImportService(provider: provider);
        },
      )
    ],
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Converter Trello para CSV',
      theme: ThemeData(
        colorSchemeSeed: mainColor,
      ),
      home: const MyHomePage(title: 'Converter Trello para CSV'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              spacing: 15,
              runSpacing: 15,
              children: [
                ElevatedButton.icon(
                  icon: const Icon(Icons.import_export_rounded),
                  onPressed: () async {
                    context.read<ImportService>().importFile();
                  },
                  label: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text('Importar Json'),
                  ),
                ),
                ElevatedButton.icon(
                  icon: const Icon(Icons.copy),
                  onPressed: () {
                    Clipboard.setData(ClipboardData(
                        text: Provider.of<FileProvider>(
                      context,
                      listen: false,
                    ).jsonContent));
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text('Texto copiado!'),
                        behavior: SnackBarBehavior.floating,
                      ),
                    );
                  },
                  label: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text('Copiar JSON para a área de transferência'),
                  ),
                ),
                ElevatedButton.icon(
                  icon: const Icon(Icons.file_download_outlined),
                  onPressed: () {
                    context.read<ImportService>().exportCsv();
                  },
                  label: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text('Exportar tabela'),
                  ),
                ),
                ElevatedButton.icon(
                  icon: const Icon(Icons.copy),
                  onPressed: null,
                  label: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text('Copiar excel para a área de transferência'),
                  ),
                ),
              ],
            ),
            const Divider(),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Expanded(
                          child: ScrollConfiguration(
                            behavior: ScrollConfiguration.of(context).copyWith(
                              dragDevices: {
                                PointerDeviceKind.mouse,
                                PointerDeviceKind.touch,
                              },
                            ),
                            child: SingleChildScrollView(
                              controller: ScrollController(),
                              scrollDirection: Axis.vertical,
                              child: SingleChildScrollView(
                                controller: ScrollController(),
                                scrollDirection: Axis.horizontal,
                                child: Consumer<FileProvider>(
                                    builder: (context, provider, _) {
                                  return DataTable(
                                    columns: const [
                                      DataColumn(
                                        label: Text('List'),
                                      ),
                                      DataColumn(
                                        label: Text('Title'),
                                      ),
                                      DataColumn(
                                        label: Text('Description'),
                                      ),
                                      DataColumn(
                                        label: Text('Points'),
                                      ),
                                      DataColumn(
                                        label: Text('Due'),
                                      ),
                                      DataColumn(
                                        label: Text('Members'),
                                      ),
                                      DataColumn(
                                        label: Text('Labels'),
                                      ),
                                      DataColumn(
                                        label: Text('Card #'),
                                      ),
                                      DataColumn(
                                        label: Text('Card URL'),
                                      ),
                                    ],
                                    rows: provider.lines
                                        .map(
                                          (line) => DataRow(
                                            cells: [
                                              DataCell(Text(line.list)),
                                              DataCell(Text(line.title.replaceAll("\n", ""))),
                                              DataCell(Text(line.description)),
                                              DataCell(Text(line.points)),
                                              DataCell(Text(line.due)),
                                              DataCell(Text(line.members)),
                                              DataCell(Text(line.labels)),
                                              DataCell(Text(line.cardId)),
                                              DataCell(Text(line.cardUrl)),
                                            ],
                                          ),
                                        )
                                        .toList(),
                                  );
                                }),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
